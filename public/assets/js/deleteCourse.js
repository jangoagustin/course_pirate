console.log("Hello from deleteCourse.js");
console.log(window.location.search);

let params = new URLSearchParams(window.location.search);

console.log(...params);
console.log(params.has('courseId'));
// console.log(params.get('courseId'));

let courseIdExists = params.has('courseId');
let courseId = params.get('courseId');

if(courseIdExists === true) {
  console.log(courseId);
  let token = localStorage.getItem('token');
  console.log(token);
  fetch(`http://localhost:4000/api/courses/${courseId}`, {
    method: 'DELETE',
    headers: {
      'Authorization':`Bearer ${token}`
    }
  }).then(res => {
    return res.json()
  }).then(data => {
    //creation of new course successful
    if(data === true) {
      alert("Course Deleted")
      //redirect to courses index ;page
      window.location.replace('./courses.html');
    } else {
      //error in creating course, redirect to error page
      alert('something went wrong')
    }
  })
} 


