console.log("I am connected to register.js");

let registerUserForm = document.querySelector("#registerUser");

registerUserForm.addEventListener("submit", (e) => {
  e.preventDefault();

  let userFirstName = document.querySelector("#firstName").value;
  let userLastName = document.querySelector("#lastName").value;
  let userEmail = document.querySelector("#userEmail").value;
  let userMobileNo = document.querySelector("#mobileNumber").value;
  let userPassword = document.querySelector("#password").value;
  let userVerifyPassword = document.querySelector("#verifyPassword").value;

  if((userPassword !== "" && userVerifyPassword !== "") && (userVerifyPassword === userPassword) && (userMobileNo.length === 11)) {
    fetch('https://thawing-atoll-78444.herokuapp.com/api/users/email-exists', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: userEmail
      })
    }).then(res => res.json()
      )
    .then(data => {
      if(data === false) {
        fetch("https://thawing-atoll-78444.herokuapp.com/api/users/register", {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            firstName: userFirstName,
            lastName: userLastName,
            email: userEmail,
            mobileNo: userMobileNo,
            password: userPassword
          })
        }).then(res => {
          return res.json();
        }).then(data => {
          console.log(data);

          if(data === true) {
            alert("New user created successfully")
          } else {
            alert("Something went wrong. KABOOM!")
          }
        })
      } else {
        alert("This email address already exists in our system. Please choose another email.")
      }
    })
  } else {
    alert("Please review your details and try again.")
  };
});