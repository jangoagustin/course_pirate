console.log("I am connected to addCourse.js");

let addCourseForm = document.querySelector("#createCourse");

addCourseForm.addEventListener("submit", (e) => {
  e.preventDefault();

  let courseName = document.querySelector("#courseName").value;
  let coursePrice = document.querySelector("#coursePrice").value;
  let courseDescription = document.querySelector("#courseDescription").value;

  if(courseName !== " " && coursePrice !== " " && courseDescription !== " ") {
    fetch('https://thawing-atoll-78444.herokuapp.com/api/courses/course-exists', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: courseName
      })
    }).then(res => res.json()
      )
    .then(data => {
      if(data === false) {
        fetch("https://thawing-atoll-78444.herokuapp.com/api/courses/addCourse", {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            name: courseName,
            price: coursePrice,
            description: courseDescription
          })
        }).then(res => {
          return res.json()
        }).then(data => {
          console.log(data);

          if(data === true) {
            alert("New course added successfully.")
          } else {
            alert("Something went wrong. KABOOM!")
          };
        });
      } else {
        alert("This course already exists in our system.")
      }
    })
  } else {
    alert("Detected invalid entries. Please try again.")
  }
});