console.log("Hello from index.js");
//capture the navSession element inside the navbar component
let navItem = document.querySelector('#navSession');

//lets take the access token from the local storage property.
let userToken = localStorage.getItem("token");
console.log(userToken);

//lets create a control structure that will determine which elements inside the navbar will be displayed if a user token is found in the local storage
if(!userToken) {
  navItem.innerHTML =
  `
  <li class="nav-item">
    <a href="./pages/login.html" class="nav-link">Log In</a>
  </li>
  `
} else {
  navItem.innerHTML =
  `
  <li class="nav-item">
    <a href="./pages/logout.html" class="nav-link">Log Out</a>
  </li>
  `
}