console.log("hello from login.js");

let loginForm = document.querySelector('#loginUser');

loginForm.addEventListener("submit", (e) => {
  e.preventDefault()

  let uEmail = document.querySelector('#userEmail').value;
  console.log(uEmail)
  let uPassword = document.querySelector('#password').value
  console.log(uPassword)

  //how can we inform a user that a blank input field cannot be logged in?
  if(uEmail == "" || uPassword == "") {
    alert("Please input your email and/or password.")
  } else {
    fetch('https://thawing-atoll-78444.herokuapp.com/api/users/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: uEmail,
        password: uPassword
      })
    }).then(res => {
      return res.json()
    }).then(data => {
      console.log(data.access)
      if(data.access) {
        //lets save the access token inside our local storage.
        localStorage.setItem('token', data.access)
        //this local storage can be found inside the subfolder of the appData of the local file of the google chrome browser inside the data module of the user folder
        alert("access key saved on local storage.") //for educational purposes only
        fetch('https://thawing-atoll-78444.herokuapp.com/api/users/details', {
          headers: {
            'Authorization': `Bearer ${data.access}`
          }
        }).then(res => {
          return res.json()
        }).then(data => {
          localStorage.setItem("id", data._id); //this came from the payload
          localStorage.setItem("isAdmin", data.isAdmin);
          console.log("Items are set inside the localStorage.");
          //direct the user to the courses page upon successful login.
          window.location.replace('../index.html');
        })
      } else {
        //if there is no existing access key value from the data variable then just inform the user. 
        alert("Something went wrong. Check your credentials.")
        //for educational purposes only.
      }
    })
  }
})