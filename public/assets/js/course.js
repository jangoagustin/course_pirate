console.log("Hello from course.js");

//The first thing that we need to do is to identify which course it needs to display inside the browser
//we are going to use the course id to identify the correct course properly
let params = new URLSearchParams(window.location.search);
//window.location -> returns a location object with information about the "current" location of the document
//.search => contains the query string section of the current URL. //QUERY STRING IS AN OBJECT //search property returns an object of type stringString
//URLSearchParams() -> this method/constructor creates and returns a URLSearchParams object (this is a class)
//"URLSearchParams" -> this describes the interface that defines utility methods to work with the query string of a URL. (this is a prop type)
//new -> instantiate a user-defined object
//yes it is a class -> template for creating the object

//params = {
// "courseId": "...id of the course that we passed"
//}
let id = params.get('courseId');
console.log(id);

//lets capture the access token from the local storage
let token = localStorage.getItem('token');
console.log(token);

//lets capture the sections of the html body
let name = document.querySelector('#courseName');
let desc = document.querySelector('#courseDesc');
let price = document.querySelector('#coursePrice');
let enroll = document.querySelector('#enrollmentContainer');

fetch(`https://thawing-atoll-78444.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(data => {
  console.log(data);

  name.innerHTML = data.name;
  desc.innerHTML = data.description;
  price.innerHTML = data.price;
  enroll.innerHTML = `<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>`;
  
  //we have to capture  first the anchor tag for enroll, then add an event listener to trigger an event.
  document.querySelector("#enrollButton").addEventListener("click", () => {
    //insert the course to our enrollments array inside the user collection.
    fetch('https://thawing-atoll-78444.herokuapp.com/api/users/enroll', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}` //we have to get the value of auth string to verify and validate the user
      },
      body: JSON.stringify({
        courseId: id
      })
    }).then(res => {
      return res.json()
    }).then(data => {
      //now we can inform the user if the request has been done or not
      if(data === true) {
        alert("Thank you for enrolling to this course");
        window.location.replace("./courses.html");
      } else {
        //inform the user that the request has failed
        alert("something went wrong.")
      }
    })
  })
})