console.log("Hello from courses.js");
let modalButton = document.querySelector('#adminButton');

//CAPTURE the html body which will display the content coming from the db.
let container = document.querySelector('#coursesContainer');
let cardFooter;
let isAdmin = localStorage.getItem("isAdmin");

if(isAdmin === "false" || !isAdmin) {
  //if a user is a regular user, do not show the addCourse button
  modalButton.innerHTML = null;
} else {
  modalButton.innerHTML = `
    <div class="col-md-2 offset-md-10">
      <a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
    </div>
  `
}

fetch('https://thawing-atoll-78444.herokuapp.com/api/courses/').then(res => res.json()).then(data => {
  console.log(data);
  //declare a variable that will display a result in the browser depending on the return
  let courseData;
  //create a control structure that will determine the value that the variable will hold.
  if(data.length < 1) {
    courseData = "No Course Available"
  } else {
    //we will iterate the courses collection and display each course inside the browser
    courseData = data.map(course => {
      //lets check the make up of each element inside the courses collection
      console.log(course._id);

      //if the user is a regular user, display the enroll button and course button
      if(isAdmin == "false" || !isAdmin) {
        cardFooter = `
        <a href="./course.html?courseId=${course._id}" class="btn btn-light btn-block text-black">View Course Details</a>
        `
      } else {
        cardFooter = `
        <a href="./editCourse.html?courseId=${course._id}" class="btn btn-light btn-block text-black">Edit</a>
        <a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-light btn-block text-black">Disable Course</a>
        `
      }

      return (
        `
        <div class="col-md-6 my-3">
          <div class="card">
            <div class="card-body">
              <h5 class="card-header text-white"> ${course.name} </h5>
              <p class="card-text text-left">
                Description: ${course.description}
              </p>
              <p class="card-text text-left">
                Price: ${course.price}
              </p>
              <p class="card-text text-left">
                Date Created: ${course.createdOn}
              </p>
            </div>

            <div class="card-footer">
              ${cardFooter}
            </div>

          </div>
        </div>
        `
      )
    }).join("") 
    //we used the join() to create a return of a new string
    //it concatenated all the objects inside the array and converted each into a string data type
    //? -> inside the URL "acts" as a "separator", it indicates the end of a URL resource path, and indicates the start of the *query string*
  }
  container.innerHTML = courseData;
})
