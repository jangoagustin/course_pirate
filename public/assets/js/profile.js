console.log("Hello from profile.js");
//get the access token in the localStorage
let token = localStorage.getItem("token");
console.log(token);

let profile = document.querySelector("#profileContainer");

//lets create a control structure that will determine the display if the access token is null or empty
if(!token || token === null) {
  //lets redirect the user to the login page
  alert("You must Login First");
  window.location.href="./login.html"
} else {
  // console.log("yes may nakuhang token") //for educational purposes only
  fetch('http://localhost:4000/api/users/details', {
    method: 'GET',
    headers: {
      'Content-Type':'application/json',
      'Authorization': `Bearer ${token}`
    }
  }).then(res => res.json())
  .then(data => {
    console.log(data);

    let enrollmentData = data.enrollments.map(classData => {
      console.log(classData)
      return(
        `
          <tr>
            <td>${classData.courseId}</td>
          </tr>
        `
      )
    }).join("") //remove the commas
    profile.innerHTML = `
      <div class="col-md-12">
        <section class="jumbotron my-5">
          <hr class="text-center">First Name: ${data.firstName}</hr>
          <hr class="text-center">Last Name: ${data.lastName}</hr>
          <hr class="text-center">Email: ${data.email}</hr>
          <table class="table">
            <thead>
              <tr>
                <th>Course ID:</th>
                <th>Enrolled On:</th>
                <th>Status:</th>
                <tbody> ${enrollmentData} </tbody>
              </tr>
            </thead>
          </table>
        </section>
      </div>
    `
  })
}